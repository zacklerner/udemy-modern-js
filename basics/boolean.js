// === - equality operator
// !== - not equal operator
// < - less than operator
// > - greater than operator
// <= - less than or equal to operator
// >= - greater than or equal to operator

// let temp = 113

// if (temp <= 32) {
//   console.log("It's freezin!")
// } else if (temp >= 110) {
//   console.log("Hothothot")
// } else {
//   console.log("U good.")
// }


// Challenge are

let myAge = 123
if (myAge <= 7) {
  console.log("Must be this tall to ride")
} else if (myAge >= 65) {
  console.log("Senior discount for you, oldy!")
} else {
  console.log("FU PAYME")
}
